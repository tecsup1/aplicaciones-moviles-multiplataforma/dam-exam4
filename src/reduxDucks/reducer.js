import {db, auth} from '../firebaConfig/firebasConfig';
import * as pedidos from '../data/pedidos.json';



const IS_LOGIN = 'IS_LOGIN';
const SIGN_OUT = 'SIGN_OUT';
const GET_RESTAURANTS = 'GET_RESTAURANTS';

const GET_CHALIUS_RESTAURANTS = 'GET_CHALIUS_RESTAURANTS';
const GET_RESERVAS = 'GET_RESERVAS';
const INSERT_RESERVA = 'INSERT_RESERVA';
const DELETE_RESERVA = 'DELETE_RESERVA';

// Reducer
/*action = {
  type: "GET_TALES",
  payload: res.data
}*/

const initialState = {
  isLoggin: false,
  restaurantsList: [],
  chaliusRestaurantsList: [],
  userEmail: {},
  reservasList: [],

  pedidosList:[],
};

export default function talesReducer(state = initialState, action = {}) {
  switch (action.type) {
    case IS_LOGIN:
      return {
        ...state,
        isLoggin: true,
        userEmail: action.payload,
      };
    case SIGN_OUT:
      return {
        ...state,
        isLoggin: false,
      };
    case GET_RESTAURANTS:
      return {
        ...state,
        restaurantsList: action.payload,
      };

    case GET_CHALIUS_RESTAURANTS:
      // console.log("reducer: "+ action.payload)
      return {
        ...state,
        chaliusRestaurantsList: action.payload,
      };

    case GET_RESERVAS:
      return {
        ...state,
        reservasList: action.payload,
      };
    case INSERT_RESERVA:
      return {
        ...state,
        reservasList: [...state.reservasList, action.payload],
      };

    case DELETE_RESERVA:
      const index = state.reservasList.findIndex(
        (n) => (n.id = action.payload),
      );
      state.reservasList.splice(index, 1);
      return {
        ...state,
      };

    default:
      return state;
  }
}

// Action Creators
export function searchResults() {
  return {
    type: IS_LOGIN,
    payload: {},
  };
}

// Redux Thunk
export function getRestaurants() {
  return async function (dispatch, getState) {
    const root = await db.collection('hoteles').get();
    const restaurants = root.docs.map((e) => e.data());
    console.log(restaurants);
    try {
      const root = await db.collection('hoteles').get();
      const restaurants = root.docs.map((e) => e.data());

      dispatch({
        type: 'GET_RESTAURANTS',
        payload: restaurants,
      });
    } catch (error) {
      console.log(error);
    }
  };
}

// export const obtenerRestaurantesAccion = () => async (dispatch, getState) => {
export function obtenerRestaurantesAccion() {
  return async function (dispatch, getState) {
    const res = pedidos.restaurants; // simulamos la petición axios
    // console.log("chalius redux: "+res.restaurants[0].restaurant_name)
    // console.log("chalius redux: "+res)
    dispatch({
      type: 'GET_CHALIUS_RESTAURANTS',
      payload: res,
    });
  };
}
export function signOut() {
  return async function (dispatch, getState) {
    try {
      await auth.signOut();
      dispatch({
        type: 'SIGN_OUT',
        payload: {},
      });
    } catch (error) {
      console.log(error);
    }
  };
}

export function getReservas(id) {
  return async function (dispatch, getState) {
    try {
      const root = await db
        .collection('reservas')
        .where('user', '==', id)
        .get();

      const reservas = root.docs.map((e) => ({...e.data(), id: e.id}));
      dispatch({
        type: 'GET_RESERVAS',
        payload: reservas,
      });
    } catch (error) {
      console.log(error);
    }
  };
}

export function setReservas(payload) {
  return async function (dispatch, getState) {
    try {
      const data = await db.collection('reservas').add(payload);
      console.log('@@@@@@@@@@@@@@@@@@@', data.id);
      dispatch({
        type: 'INSERT_RESERVA',
        payload: {...payload, id: data.id},
      });
    } catch (error) {
      console.log(error);
    }
  };
}

export function deleteReserva(id) {
  return async function (dispatch, getState) {
    try {
      console.log('@@@@@@@@@@', id);
      await db.collection('reservas').doc(id).delete();
      dispatch({
        type: 'DELETE_RESERVA',
        payload: id,
      });
    } catch (error) {
      console.log(error);
    }
  };
}

// pedidos:

export function setPedido(payload) {
  return async function (dispatch, getState) {
    try {
      const data = await db2.collection('pedidos').add(payload);
      console.log('apadiendo pedido', data.id);
      dispatch({
        type: 'INSERT_PEDIDO',
        payload: {...payload, id: data.id},
      });
    } catch (error) {
      console.log(error);
    }
  };
}