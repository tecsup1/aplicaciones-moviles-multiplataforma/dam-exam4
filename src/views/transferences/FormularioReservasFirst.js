import React, { useState } from "react";
import { useSelector } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { Icon, Input } from "react-native-elements";
moment.locale("es");
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";

const REG_NUMBER = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;
const REG_LETTERS = /^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$/;

export default function TransferenceFirst({ navigation, route }) {

  const { email, uid } = useSelector((state) => state.reducer.userEmail);

  const [date, setDate] = useState({
    nombre: "",
    celular: "",
    // fecha: "",
    tiempo: "",
    personas: "",
  });
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);

  const [fecha, setFecha] = useState("")
  // ERRORS STATE
  const [errName, setErrName] = useState("");
  const [errCell, setErrCell] = useState("");
  const [errNumberPerson, setErrNumberPerson] = useState("");
  const [errTime, setErrTime] = useState("");
  const [errDate, setErrDate] = useState("");

  //  FECHA
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    hideDatePicker();
    const fecha = moment(date).format("DD/MM/YYYY");
    setFecha(fecha);
    setErrDate("");
  };

  // TIEMPO
  const showTimePicker = () => {
    setTimePickerVisibility(true);
  };

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };

  const handleConfirmTime = (time) => {
    hideTimePicker();
    const tiempo = moment(time).format("hh:mm:ss a");
    setDate({ ...date, tiempo });
    setErrTime("");
  };

  // VALIDATORS
  const nameValidator = (text) => {
    if (!text) return setErrName("Complete el campo");
    if (!REG_LETTERS.test(text)) return setErrName("Solo letras");
    else {
      setDate({ ...date, nombre: text });
      setErrName("");
    }
  };

  const telefonoValidator = (text) => {
    if (!text) return setErrCell("Complete el campo");
    if (!REG_NUMBER.test(text)) return setErrCell("Celular inválido");
    if (text.length != 9) return setErrCell("Solo números de 9 digitos");
    else {
      setDate({ ...date, celular: text });
      setErrCell("");
    }
  };

  const personValidator = (text) => {
    if (!text) return setErrNumberPerson("Complete el campo");
    if (!REG_NUMBER.test(text)) return setErrNumberPerson("Solo números");
    else {
      setDate({ ...date, personas: text });
      setErrNumberPerson("");
    }
  };

  const navigationValidator = () => {
    if (fecha == "" && date.tiempo == "") {
      setErrDate("Seleccione una fecha");
      setErrTime("Seleccione una hora");
    } 
    else if (date.tiempo == "") return setErrTime("Seleccione una hora");
    else if (fecha == "") return setErrDate("Seleccione una fecha");
    else return navigation.navigate("Second", { ...date, fecha, email, restaurante: route.params.restaurante, user: uid });
    
  };

  return (
    <KeyboardAwareScrollView
      style={{
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor:"white"
      }}
    >
      <Text style={{
        alignSelf: "center",
        marginTop:30,
        fontFamily: "OpenSans-ExtraBold",
        marginBottom:15,
        fontSize: 20,
        color: "#24AADD"
        }}
      >
        FORMULARIO DE RESERVA
      </Text>
      <View style={{ marginVertical: -5 }}>
        <Input
          leftIcon={
            <Icon name="account" type="material-community" color="#24AADD" />
          }
          placeholder="Nombre"
          errorStyle={{ color: "red" }}
          errorMessage={errName}
          onChangeText={nameValidator}
        />
      </View>
      <View style={{ marginVertical: -5 }}>
        <Input
          leftIcon={
            <Icon name="email" type="material-community" color="#24AADD" />
          }
          placeholder="Email"
          value={email}
        />
      </View>
      <View style={{ marginVertical: -5 }}>
        <Input
          leftIcon={<Icon name="phone" type="entypo" color="#24AADD" />}
          placeholder="Celular"
          errorMessage={errCell}
          onChangeText={telefonoValidator}
        />
      </View>
      <View style={{ marginVertical: -5 }}>
        <TouchableOpacity onPress={showDatePicker} style={{ width: "100%" }}>
          <Input
            leftIcon={<Icon name="date" type="fontisto" color="#24AADD" />}
            value={fecha ? fecha : "Seleccione una fecha"}
            disabled
            errorMessage={errDate}
          />
        </TouchableOpacity>
      </View>
      <View style={{ marginVertical: -5, flexDirection: "row" }}>
        <TouchableOpacity onPress={showTimePicker} style={{ width: "100%" }}>
          <Input
            leftIcon={<Icon name="ios-clock" type="ionicon" color="#24AADD" />}
            value={date.tiempo ? date.tiempo : "Hora para la reservación"}
            disabled
            errorMessage={errTime}
          />
        </TouchableOpacity>
      </View>
      <View style={{ marginVertical: -5 }}>
        <Input
          leftIcon={<Icon name="users" type="font-awesome" color="#24AADD" />}
          placeholder="Indique cuantas personas"
          onChangeText={personValidator}
          errorMessage={errNumberPerson}
        />
      </View>

      {/* Ttiempo */}
      <View style={{ alignItems: "center" }}>
        <DateTimePickerModal
          isVisible={isTimePickerVisible}
          mode="time"
          onConfirm={handleConfirmTime}
          onCancel={hideTimePicker}
        />
        <DateTimePickerModal
          isVisible={isDatePickerVisible}
          mode="date"
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
        />
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={[styles.button, { backgroundColor: "#50E2B5" }]}
            onPress={() => navigation.goBack()}
          >
            <Text style={{ textAlign: "center", color: "white", fontWeight: "bold" }}>ATRAS</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.button, { backgroundColor: "#662BFF" }]}
            onPress={navigationValidator}
          >
            <Text style={{ textAlign: "center", color: "white", fontWeight: "bold" }}>
              CONTINUAR
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  button: {
    margin: 15,
    height: 40,
    justifyContent: "center",
    width: 120,
    backgroundColor: "#428AF8",
    borderRadius: 30,
  },
});
