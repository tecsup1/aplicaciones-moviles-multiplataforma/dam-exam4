import moment from "moment";
moment.locale("es");

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Text, View, TouchableOpacity, StyleSheet,Dimensions } from "react-native";
import { setReservas } from "../../reduxDucks/reducer";
import { Card, ListItem, Icon, Divider } from "react-native-elements";

const { width, height } = Dimensions.get("window");

export default function TransferenceSecond({ navigation, route }) {
  // const [importe, setImporte] = useState(0)
  const dispatch = useDispatch();
  console.log(route.params);
  const { nombre, email, celular, fecha, tiempo, personas } = route.params;

  const data = route.params;
  const sendReserva = () => {
    dispatch(setReservas(data));
    navigation.navigate("Third");
  };

  return (
    <View
      style={{
        flex: 1,
        height:height
      }}
    >
      <Card
        title={"Confirmar datos de Reserva"}
        containerStyle={{ height: height-55}}
        titleStyle={{fontSize:30,color:"#24AADD"}}
      >
        <View style={{ alignItems: "center" }}>
          <Text style={{ fontWeight: "bold",fontSize:20 }}>NOMBRE:</Text>
          <Text style={{ marginBottom: 8, fontSize:17, color:"gray" }}>{nombre}</Text>

          <Text style={{ fontWeight: "bold",fontSize:20 }}>EMAIL:</Text>
          <Text style={{ marginBottom: 8,fontSize:17, color:"gray" }}>{email}</Text>

          <Text style={{ fontWeight: "bold",fontSize:20 }}>CELULAR:</Text>
          <Text style={{ marginBottom: 8,fontSize:17, color:"gray" }}>{celular}</Text>

          <Text style={{ fontWeight: "bold",fontSize:20 }}>FECHA:</Text>
          <Text style={{ marginBottom: 8,fontSize:17, color:"gray" }}>{fecha}</Text>

          <Text style={{ fontWeight: "bold",fontSize:20 }}>HORA DE RESERVACION:</Text>
          <Text style={{ marginBottom: 8,fontSize:17, color:"gray" }}>{tiempo}</Text>

          <Text style={{ fontWeight: "bold",fontSize:20 }}>PERSONAS:</Text>
          <Text style={{ marginBottom: 8,fontSize:17, color:"gray" }}>{personas}</Text>

          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={[styles.button, { backgroundColor: "#50E2B5" }]}
              onPress={() => navigation.goBack()}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "white",
                  fontWeight: "bold",
                }}
              >
                ATRAS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
            style={[styles.button, { backgroundColor: "#662BFF" }]}
            onPress={sendReserva}
          >
            <Text
              style={{
                textAlign: "center",
                color: "white",
                fontWeight: "bold",
              }}
            >
              CONFIRMAR
            </Text>
          </TouchableOpacity>
          </View>
          
        </View>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    margin: 15,
    height: 40,
    justifyContent: "center",
    width: 120,
    backgroundColor: "#428AF8",
    borderRadius: 30,
  },
});
