import moment from "moment";
moment.locale("es");

import React, { useState } from "react";
import { CommonActions } from "@react-navigation/native";
import { Text, View, Button, Image, TouchableOpacity } from "react-native";

import source from "../../assets/login/logohotel.png";

export default function TransferenceThrid({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#121212",
      }}
    >
      <View
        style={{
          width: "100%",
          flex: 1,
          alignSelf: "center",
          justifyContent: "center",
        }}
      >
        <Image
          source={source}
          style={{
            width: "100%",
            height: "50%",
            alignSelf: "center",
            marginBottom: 20,
          }}
        />
        <Text
          style={{
            color: "white",
            marginBottom:20,
            textAlign:"center",
            fontFamily: "Satisfy-Regular",
            fontSize: 25,
          }}
        >
          Gracias por su Reserva ... !!!
        </Text>

        <TouchableOpacity
          style={{
            alignItems: "center",
            backgroundColor: "#50E2B5",
            padding: 10,
            width: 200,
            borderRadius: 10,
            marginBottom: 10,
            alignSelf: "center",
            // opacity: 0.5
          }}
          onPress={() => navigation.navigate("Reservas")}
        >
          <Text style={{ color: "white", fontWeight: "bold" }}>
            VER RESERVAS
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
