import React, {Component} from 'react';
import {View, Text, Image, Alert, StyleSheet, Dimensions,ScrollView} from 'react-native';
import Video from 'react-native-video';
const {width, height} = Dimensions.get('window');

class InicioView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ScrollView>
        <Text
          style={{
            fontSize: 30,
            textAlign: 'center',
            color: '#568B2F',
            fontWeight: 'bold',
          }}>
          Bienvenido a EasyFood
        </Text>
        <View style={{justifyContent: 'center', backgroundColor: 'black',marginTop:35}}>
          <Video
            // source={{
            //   uri: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
            // }}
            source={require('../../assets/videos/video1.mp4')}
            controls={true}
            //fullscreen={false}
            resizeMode="cover"
            style={{width: '100%', height: 300}}
          />
        </View>
          <Text style={{fontSize: 25,marginTop:150, margin: 20, color: '#568B2F'}}>
            En Easy food queremos que tengas todas las facilidades para reservar
            tu restaurante favorito y para poder hacer todos los pedidos que
            desees.
          </Text>
       
      </ScrollView>
    );
  }
}

var styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default InicioView;
