import React, {Component} from 'react';
import {View, Text, Image, Alert, Button} from 'react-native';

//redux
import Login from './LoginComponent/Login';
import Registro from './LoginComponent/Registro';
import TabMenuContainer from './TabMenuContainer';

// import InicioView from './inicioComponents/InicioView';
import PedidoView from './pedidosComponents/PedidoView';
// import ReservasView from './reservasComponents/ReservasView';

// react navigation:
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, HeaderBackButton} from '@react-navigation/stack';
import DetailReserva from './reservasComponents/Details';
import MapPedidosView from './pedidosComponents/MapPedidosView';
import First from './transferences/FormularioReservasFirst';
import Second from './transferences/FormularioReservasSecond';
import Third from './transferences/FormularioReservasThird';
import HacerPedidoView from './pedidosComponents/HacerPedidoView';

import MaterialMenu from './MaterialMenu';

// REDUX ACTIONS
import {signOut} from '../reduxDucks/reducer';

import {connect} from 'react-redux';
const Stack = createStackNavigator();
const StackLogin = createStackNavigator();

function LoginStackScreens() {
  return (
    <StackLogin.Navigator>
      <StackLogin.Screen
        name="Login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <StackLogin.Screen
        name="Registro"
        component={Registro}
        options={{
          headerShown: false,
        }}
      />
    </StackLogin.Navigator>
  );
}

function LogoTitle() {
  return (
    <View
      style={{
        flex: 1,
        height: 40,
        alignItems: 'flex-start',
      }}>
      <Image
        style={{
          flex: 1,
          width: 100,
          height: null,
          resizeMode: 'contain',
        }}
        source={require('../assets/header/headerhotel.png')}
      />
    </View>
  );
}

class MainStackContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log(this.props.isSignedIn);
    return (
      <NavigationContainer>
        <Stack.Navigator>
          {this.props.isSignedIn ? (
            <>
              <Stack.Screen
                name="TabMenuContainer"
                component={TabMenuContainer}
                options={{
                  // headerRight: () => <Button title="CS" {...props} onPress={this.props.signOut}/>,
                  headerRight: () => (
                    <MaterialMenu signOut={this.props.signOut} />
                  ),
                  headerStyle: {
                    backgroundColor: '#121212',
                    height: 40,
                  },
                  headerTitle: (props) => <LogoTitle {...props} />,
                }}
              />

              <Stack.Screen
                name="PedidoView"
                component={PedidoView}
                options={{
                  title: 'Pedido View',
                  headerTintColor: 'white',
                  headerStyle: {
                    backgroundColor: '#568B2F',
                    height: 55,
                  },
                }}
              />
              <Stack.Screen
                name="DetailReserva"
                component={DetailReserva}
                options={{
                  title: 'Pedido View',
                  headerTintColor: 'white',
                  headerStyle: {
                    backgroundColor: '#121212',
                    height: 55,
                  },
                }}
              />
              <Stack.Screen
                name="First"
                component={First}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="Second"
                component={Second}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="Third"
                component={Third}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="MapPedidosView"
                component={MapPedidosView}
                options={{
                  title: 'Map Pedidos View',
                  headerTintColor: 'white',
                  headerStyle: {
                    backgroundColor: 'black',
                    height: 55,
                  },
                }}
              />

              <Stack.Screen
                name="HacerPedidoView"
                component={HacerPedidoView}
                options={{
                  title: 'Hacer pedido',
                  headerTintColor: 'white',
                  headerStyle: {
                    backgroundColor: 'black',
                    height: 55,
                  },
                }}
              />
            </>
          ) : (
            <>
              <Stack.Screen
                name="Login"
                component={LoginStackScreens}
                options={{
                  headerShown: false,
                }}
              />
            </>
          )}

          {/* DESPUES DE ESTA LINEA AÑADIR SUS COMPONENTES AL STACK*/}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const props = (state) => ({
  isSignedIn: state.reducer.isLoggin,
});

const actions = {
  signOut,
};

export default connect(props, actions)(MainStackContainer);
