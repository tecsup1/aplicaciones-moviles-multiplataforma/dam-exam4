import React, { Component } from "react";
import { View, Text, Image, Alert } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
const Tab = createMaterialBottomTabNavigator();

import InicioView from "./inicioComponents/InicioView";
import PedidosView from "./pedidosComponents/PedidosView";
import ReservasView from "./reservasComponents/ReservasView";
import ReservasList from "./reservasComponents/Reservas";
import "react-native-gesture-handler";

class TabMenuContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#f0edf6"
        inactiveColor="#F4D52A"
        barStyle={{ backgroundColor: "#121212", }}
        tabBarOptions={{
          activeTintColor: "#e91e63",
        }}
      >
        {/* <Tab.Screen
          name="InicioView"
          component={InicioView}
          options={{
            tabBarLabel: "Inicio",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={25} />
            ),
          }}
        /> */}
        <Tab.Screen
          name="Restaurantes"
          component={ReservasView}
          options={{
            tabBarLabel: "Hoteles",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="wallet-travel" color={color} size={25} />
            ),
          }}
        />
        <Tab.Screen
          name="PedidosView"
          component={PedidosView}
          options={{
            tabBarLabel: "Pedidos",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="tortoise" color={color} size={25} />
            ),
          }}
        />

        <Tab.Screen
          name="Reservas"
          component={ReservasList}
          options={{
            tabBarLabel: "Reservas",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="laptop" color={color} size={25} />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}

export default TabMenuContainer;
