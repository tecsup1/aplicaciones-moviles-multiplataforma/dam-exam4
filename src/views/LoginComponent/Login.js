import React, { useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
} from "react-native";
import { auth } from "../../firebaConfig/firebasConfig";
import { useDispatch, useSelector } from "react-redux";
const { width, height } = Dimensions.get("window");

export default function App({ navigation }) {
  const dispatch = useDispatch();
  const [form, useForm] = useState({
    email: "",
    password: "",
  });

  const sendData = async () => {
    try {
      const {
        user: { email, uid },
      } = await auth.signInWithEmailAndPassword(form.email, form.password);
      dispatch({
        type: "IS_LOGIN",
        payload: {
          email,
          uid,
        },
      });
    } catch (err) {
      alert(err)
    }
    // auth.signInWithEmailAndPassword(form.email, form.password)
    //   .then(({ user: { email, uid } }) => {
    //     console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@QQ")
    //     dispatch({
    //     type: "IS_LOGIN",
    //     payload: {
    //       email,
    //       uid
    //     }
    //   })
    //   navigation.navigate("TabMenuContainer")
    // })
    //   .catch(err => {
    //     console.log(err)
    //   })
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require("../../assets/login/logohotel.png")}
      />
      <Text style={styles.titulo}>Inicio de Sesión</Text>

      <TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={(text) => useForm({ ...form, email: text })}
      />

      <TextInput
        secureTextEntry={true}
        style={styles.input}
        placeholder="Contraseña"
        onChangeText={(text) => useForm({ ...form, password: text })}
      />

      <TouchableOpacity style={styles.button}>
        <Text style={{ textAlign: "center", fontWeight:"bold" }} onPress={sendData}>
          INICIAR SESION
        </Text>
      </TouchableOpacity>

      <TouchableOpacity style={[styles.button, { backgroundColor: "#B245C1" }]}>
        <Text
          style={{ textAlign: "center", color: "white", fontWeight:"bold" }}
          onPress={() => navigation.navigate("Registro")}
        >
          REGISTRAR
        </Text>
      </TouchableOpacity>

      <Text style={{ fontSize: 13, color: "white" }}>
        No tienes una cuenta?
        <Text style={{ fontSize: 13, color: "#428AF8" }}>Registrate</Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    flex: 1,
    backgroundColor: "#121212",
  },
  logo: {
    width: width,
    height: height / 2,
  },
  titulo: {
    marginTop: 20,
    fontSize: 33,
    color: "white",
    //fontWeight:"bold",
    fontFamily:"Satisfy-Regular"
  },
  input: {
    backgroundColor: "white",
    borderRadius: 10,
    marginVertical: 15,
    borderWidth: 2,
    textAlign: "center",
    color: "#428AF8",
    height: 37,
    width: width / 2,
  },
  button: {
    backgroundColor: "#F4D52A",
    borderRadius: 10,
    justifyContent: "center",
    width: width / 2,
    textAlign: "center",
    height: 37,
    marginBottom: 15,
  },
});
