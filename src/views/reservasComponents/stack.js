import React, { Component } from "react";
import {
  View,
  Text
} from 'react-native'
import Detail from './Details'
import Reservas from './ReservasView'
import {
  createStackNavigator,
  HeaderBackButton,
} from "@react-navigation/stack";

const Stack = createStackNavigator()

export default () => {
  return (
    <Stack.Navigator initialRouteName="Reservas">
      <Stack.Screen
        name="Reservas"
        component={Reservas}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Details"
        component={Detail}
      />
    </Stack.Navigator>
  )
}