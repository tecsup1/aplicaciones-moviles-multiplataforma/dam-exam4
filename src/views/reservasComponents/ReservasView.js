import React, { Component } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  Touchableop,
} from "react-native";
import { ListItem } from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
// import LinearGradient from "react-native-linear-gradient";
import { connect } from "react-redux";
import { getRestaurants } from "../../reduxDucks/reducer";
import { Rating, AirbnbRating } from 'react-native-elements';

class ReservasView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  async componentDidMount() {
    const { getRestaurants } = this.props
    getRestaurants()
    console.log("restaurantes papurro: "+this.props.restaurants)
  }

  keyExtractor = (item, index) => index.toString();

  renderItem = ({ item }) => (
    <ListItem
      Component={TouchableScale}
      friction={90} //
      tension={100} // These props are passed to the parent component (here TouchableScale)
      activeScale={0.95} //
      title={item.nombre}
      titleStyle={{ color: "black", fontWeight: "bold" }}
      subtitle={item.direccion}
      subtitleStyle={{ color: "black" }}
      subtitleProps={{ numberOfLines: 2, ellipsizeMode: "tail", width: 20 }}
      leftAvatar={{  
        source:{uri:item.imagen},
        width: 100,
        height: 100,
        borderRadius: 500 / 2,
      }}
      bottomDivider
      rightTitle={()=><Rating imageSize={10} style={{width:47}} showRating ratingColor="#F44336" type="custom" fractions={1} startingValue={item.rating} />}
      chevron={{ color: "black" }}
      onPress={() =>
        this.props.navigation.navigate("DetailReserva", {
          imagen: item.imagen,
          nombre: item.nombre,
          direccion: item.direccion,
          estado: item.estado,
          rating: item.rating,
          telefono: item.telefono,
          latitud: item.ubicacion.U,
          longitud: item.ubicacion.k,
        })
      }
    />
    
  );

  render() {
    return (
      <View style={styles.container}>
          {/* <Text>asdasdasd{JSON.stringify(this.props.restaurants)}</Text> */}
        
        <FlatList
          keyExtractor={this.keyExtractor}
          data={this.props.restaurants}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: "gray",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

const mapStateToProps = (state) => ({
  restaurants: state.reducer.restaurantsList,
});

const mapDispatchToProps = {
  getRestaurants,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReservasView);
