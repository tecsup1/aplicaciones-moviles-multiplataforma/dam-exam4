import React, {useEffect} from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  Touchableop,
  Button,
  ScrollView,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import TouchableScale from 'react-native-touchable-scale';
// import LinearGradient from "react-native-linear-gradient";
import {connect} from 'react-redux';
import {getRestaurants} from '../../reduxDucks/reducer';
import {Rating, AirbnbRating, Icon} from 'react-native-elements';
import {db} from '../../firebaConfig/firebasConfig';
import {useSelector, useDispatch} from 'react-redux';
import {getReservas, deleteReserva} from '../../reduxDucks/reducer';

import {TouchableOpacity} from 'react-native-gesture-handler';

import {
  obtenerPedidosAction,
  deletePedidoAction,
} from '../../reduxDucks/restaurantDucks';

export default () => {
  const {uid} = useSelector((state) => state.reducer.userEmail);
  const dispatch = useDispatch();
  const reservas = useSelector((state) => state.reducer.reservasList);
  const pedidos = useSelector((store) => store.restaurantDucks.pedidosArray);

  useEffect(() => {
    dispatch(getReservas(uid));
  }, [reservas]);

  const keyExtractor = (item, index) => index.toString();

  const renderItem = ({item}) => (
    <ListItem
      title={item.restaurante}
      subtitle={item.fecha}
      bottomDivider
      rightIcon={
        <Icon
          name="delete-circle"
          type="material-community"
          color="red"
          onPress={() => dispatch(deleteReserva(item.id))}
        />
      }
      chevron
    />
  );

  const renderItemPedido = ({item}) => (
    <ListItem
      title={item.nombre_pedido}
      subtitle={"S/."+item.precio}
      // rightElement={item.cantidad}
      rightSubtitle={'Cantidad ' + item.cantidad}
      bottomDivider
      rightIcon={
        <Icon
          name="delete-circle"
          type="material-community"
          color="red"
          onPress={() => dispatch(deletePedidoAction(item.id))}
        />
      }
      chevron
    />
  );

  // chalius:
  useEffect(() => {
    dispatch(obtenerPedidosAction());
  }, [pedidos]);

  return (
    <ScrollView>
      <Text style={{margin: 10}}>Reservas:</Text>
      <FlatList
        keyExtractor={keyExtractor}
        data={reservas}
        renderItem={renderItem}
      />

      <Text style={{margin: 10}}>Pedidos:</Text>
      <FlatList
        keyExtractor={keyExtractor}
        data={pedidos}
        extraData={pedidos}
        renderItem={renderItemPedido}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'gray',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
