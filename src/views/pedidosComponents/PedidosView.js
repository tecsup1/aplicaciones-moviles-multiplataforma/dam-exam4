import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import * as pedidos from '../../data/pedidos.json';

import {SearchBar} from 'react-native-elements';

const colorArray = ['#ED4B82', '#FF8E01', '#568B2F', '#FF8E01'];

//redux:
import {connect} from 'react-redux';
import {obtenerRestaurantesAccion} from '../../reduxDucks/reducer';

function ComboItem(props) {
  return (
    <TouchableOpacity
      style={{}}
      onPress={() => {
        props.goPedidoView(props.item, props.index);
      }}>
      <View style={{width: 310, flex: 4, height: 400}}>
        <Image
          style={{width: '97%', height: 400, flex: 1}}
          source={{uri: props.item.url}}
          resizeMode="stretch"
        />
      </View>
      <View
        style={{
          flex: 5,
          backgroundColor: colorArray[props.index],
          width: '97%',
        }}>
        <Text
          style={{fontSize: 30, color: 'white', marginLeft: 15, width: '85%'}}
          numberOfLines={1}>
          {props.item.combo_name}
        </Text>
        <Text style={{fontSize: 25, color: 'white', marginLeft: 15}}>
          {props.item.price}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

function RestaurantItem(props) {
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <Text style={{fontSize: 27, marginLeft: 10, flex: 3}}>
          {props.item.restaurant_name}
        </Text>
        <TouchableOpacity
          style={[my_styles.button, {flex: 1}]}
          onPress={() => {
            props.goMapView(props.item);
          }}>
          <Text style={{fontSize: 18, color: 'white', fontWeight: 'bold'}}>
            Ver mapa
          </Text>
        </TouchableOpacity>
      </View>
      <FlatList
        horizontal
        data={props.item.combos}
        renderItem={({item, index}) => (
          <ComboItem
            item={item}
            index={index}
            goPedidoView={props.goPedidoView}
          />
        )}
        flexDirection="row"
        contentContainerStyle={{paddingBottom: 50}}
        keyExtractor={(item, index) => item.combo_id.toString()}
      />
    </View>
  );
}

class PedidosView extends Component {
  constructor(props) {
    super();
    this.state = {
      search: '',
      // dataSource: [],
    };

    this.arrayholder = [];
  }



  async componentDidMount() {
    this.arrayholder = this.props.restaurants
    // redux:
    const {obtenerRestaurantesAccion} = this.props
    await obtenerRestaurantesAccion()
    this.setState({dataSource: this.props.restaurants})
    this.arrayholder = this.props.restaurants
  }

  SearchFilterFunction(text) {
    //passing the inserted text in textinput
     const newData = this.arrayholder.filter(function (item) {
      //applying filter for the inserted text in search bar
      const itemData = item.restaurant_name
        ? item.restaurant_name.toUpperCase()
        : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      search: text,
    });
  }

  goPedidoView = (item, index_color) => {
    this.props.navigation.navigate('PedidoView', {
      item: item,
      index_color: index_color,
    });
  };

  goMapView = (item) => {
    this.props.navigation.navigate('MapPedidosView', {
      item: item,
    });
  };

  render() {
    // console.log("holi: ", this.props.restaurants)
    return (
      <View style={{flex: 1.2}}>
        <View style={{flexDirection: 'row'}}>
          {/* <Text style={{fontSize: 30, margin: 15}}>Buscar tienda:</Text> */}
          <View
            style={{
              margin: 5,
              backgroundColor: 'white',
              flex: 1,
              padding: 5,
            }}>
            <SearchBar
              round
              searchIcon={null}
              placeholder="Buscar restaurant..."
              lightTheme={true}
              onChangeText={(text) => this.SearchFilterFunction(text)}
              onClear={(text) => this.SearchFilterFunction('')}
              value={this.state.search}
            />
          </View>
        </View>
        <View>
          <FlatList
            data={this.state.dataSource}

            renderItem={({item}) => (
              // <TaleItem item={item} goTaleView={this.goTaleView} />
              // <Text>{item.restaurant_name}</Text>
              <RestaurantItem
                item={item}
                goPedidoView={this.goPedidoView}
                goMapView={this.goMapView}
              />
            )}
            contentContainerStyle={{paddingBottom: 50}}
            keyExtractor={(item, index) => item.restaurant_id.toString()}
          />
        </View>
      </View>
    );
  }
}

const my_styles = StyleSheet.create({
  button: {
    backgroundColor: '#FF8E01',
    marginBottom: 15,
    alignItems: 'center',
    width: '40%',
    borderRadius: 15,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 4,
    },

    shadowOpacity: 0.5,
    shadowRadius: 4.65,
    elevation: 4,
  },
});

// export default PedidosView;
const mapStateToProps = (state) => ({
  restaurants: state.reducer.chaliusRestaurantsList,
});

const mapDispatchToProps = {
  obtenerRestaurantesAccion,
};

export default connect(mapStateToProps, mapDispatchToProps)(PedidosView);
