import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  FlatList,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

const colorArray = ['#ED4B82', '#FF8E01', '#568B2F'];

class PedidoView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  goHacerPedidoView = (item) => {
    this.props.navigation.navigate('HacerPedidoView', {
      item: item,
    });
  };


  render() {
    const {combo_name, description, price, url} = this.props.route.params.item;
    const {index_color} = this.props.route.params;
    const item = this.props.route.params.item;
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 0.98,
            backgroundColor: '#568B2F',
            marginHorizontal: 15,
            marginTop: 15,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
          }}>
          <View style={{flex: 0.55, margin: 15}}>
            <Image
              source={{uri: url}}
              style={{width: '100%', height: '100%'}}
            />
          </View>
          <Text
            style={{
              flex: 0.1,
              fontSize: 35,
              textAlign: 'center',
              margin: 5,
              color: 'white',
            }}>
            {combo_name}
          </Text>
          <Text
            style={{
              flex: 0.3,
              marginHorizontal: 20,
              fontSize: 25,
              color: 'white',
            }}>
            {description}
          </Text>
          <Text
            style={{
              flex: 0.1,
              marginHorizontal: 20,
              fontSize: 30,
              color: 'white',
            }}>
            S/. {price}
          </Text>
          <TouchableOpacity
            style={{
              flex: 0.05,
              backgroundColor: '#FF8E01',
              marginBottom: 15,
              alignSelf: 'center',
              width: '40%',
              borderRadius: 15,
              padding: 10,
              shadowColor: '#000',
              shadowOffset: {
                width: 4,
                height: 4,
              },

              shadowOpacity: 0.5,
              shadowRadius: 4.65,
              elevation: 4,
            }}
            onPress={()=>{this.goHacerPedidoView(item)}}

            >
            <Text style={{textAlign: 'center', color: 'white', fontSize: 20}} >
              Hacer pedido
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default PedidoView;
